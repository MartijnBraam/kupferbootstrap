# kupferbootstrap

## Install
Install Docker and Python 3 and put `bin/` into your `PATH`.  
Then use `kupferbootstrap`.

## Develop
Put `dev` into `version.txt` to always rebuild kupferboostrap from this directory and use `kupferbootstrap` as normal.
